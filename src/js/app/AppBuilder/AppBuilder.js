import Loader from "../../loaders/loader";

export default class AppBuilder{

    constructor(viewer){
        this.viewer = viewer;
        this.loader = new Loader();
        this.stateExterior = true;
    }

    init() {
        this.loader.loadModel(this.viewer.scene);

        document.addEventListener('click', (event) => {

            if (event.target.id === 'interior' && this.stateExterior) {
                this.stateExterior = !this.stateExterior;
                this.viewer.controls.saveState();
                this.viewer.controls.object.position.set(-4.187202615011037, 1.4862388754928442, -6.545912602037774);
                this.viewer.controls.target.set(-4.187202615011037 + 0.01, 1.4862388754928442, -6.545912602037774 + 0.01);

                document.getElementById('interior').setAttribute('disabled', '');
                document.getElementById('exterior').removeAttribute('disabled');
            }

            if (event.target.id === 'exterior' && !this.stateExterior) {
                this.stateExterior = !this.stateExterior;
                this.viewer.controls.reset();
                document.getElementById('exterior').setAttribute('disabled', '');
                document.getElementById('interior').removeAttribute('disabled');
            }
        });
    }

}
