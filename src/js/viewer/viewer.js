import {
	AmbientLight,
	Color,
	DirectionalLight, DoubleSide, Group,
	HemisphereLight,
	Mesh, MeshBasicMaterial, MeshPhongMaterial, Object3D,
	PerspectiveCamera, PlaneGeometry,
	Scene,
	SphereGeometry,
	WebGLRenderer,

} from "three";
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";

export default class Viewer{
	constructor(){
		window.viewer = this;
		this.init();
	}

	init(){
		// CONTAINER
		this.container = document.createElement( 'div' );
		this.container.id = 'threeJS';
		document.body.appendChild( this.container );

		// CAMERA
		this.camera = new PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 2000 );
		this.camera.position.set(-43, 11, -22);

		// SCENE
		window.scene = this.scene = new Scene();

		// RENDERER
		this.renderer = new WebGLRenderer( { antialias: true, alpha: true } );
		this.renderer.setPixelRatio( window.devicePixelRatio );
		this.renderer.setSize( window.innerWidth, window.innerHeight );
		this.renderer.shadowMap.enabled = true;
		// this.renderer.shadowMap.type = THREE.BasicShadowMap; // default THREE.PCFShadowMap
		// this.renderer.shadowMap.type = THREE.PCFSoftShadowMap; // default THREE.PCFShadowMap
		// this.renderer.shadowMap.type = THREE.VSMShadowMap;
		this.renderer.shadowMap.type = THREE.PCFShadowMap;


		// LIGHT
		this.initLight();

		// CONTROLS
		this.initControls();

		// ADD ON SCENE
		this.container.appendChild( this.renderer.domElement );

		// RESIZE EVENT
		window.addEventListener('resize', this.onWindowResize.bind(this));

		this.animate()
	}

	initControls(){
		this.controls = new OrbitControls( this.camera, this.renderer.domElement );
		// this.controls.enablePan = false;
	}

	initLight(){
		// const light = new AmbientLight( 0xffffff, 0.45 );
		const light = new HemisphereLight( 0xffffff, 0x444444, 0.55 );
		light.name = 'HemisphereLight';
		light.position.set( 0, 300, 0 );
		this.scene.add( light );

		// const directionalLight = new THREE.SpotLight(0xffffff, 2.3, 0, 2);
		// const directionalLight = new THREE.PointLight(0xffffff, 2.3, 400);
		const directionalLight = new DirectionalLight(0xffffff, 0.7);
		directionalLight.name = 'DirectionalLight';
		directionalLight.position.set(0, 18, 0);
		directionalLight.castShadow = true;
		directionalLight.shadow.camera = this.camera.clone();
		directionalLight.shadow.mapSize.width = 2048;
		directionalLight.shadow.mapSize.height = 2048;

		const g = new Group();
		g.position.z = 20;
		this.scene.add(g);
		//
		// const p1 = new Mesh(
		// 	new PlaneGeometry(8, 8, 100, 100),
		// 	new MeshPhongMaterial({side: DoubleSide})
		// );
		// p1.name = 'p1';
		// p1.position.set(-8.64, -1.66, -16.68);
		// p1.rotation.x = 2;
		// p1.receiveShadow = true;
		// g.add(p1);
		//
		//
		// const p2 = new Mesh(
		// 	new PlaneGeometry(5, 5, 100, 100),
		// 	new MeshPhongMaterial({side: DoubleSide})
		// );
		// p2.name = 'p2';
		// p2.position.set(-8.64, 1.66, -12.68);
		// p2.rotation.x = 2;
		// p2.castShadow = true;
		// g.add(p2);

		directionalLight.shadow.mapSize.width = 8128;
		directionalLight.shadow.mapSize.height = 8128;
		directionalLight.shadow.mapSize.width = 4096;
		directionalLight.shadow.mapSize.height = 4096;
		directionalLight.shadow.camera.near = 5;       // default
		directionalLight.shadow.camera.far = 200;      // default
		directionalLight.shadow.camera.right = 2;
		directionalLight.shadow.camera.left = -2;
		directionalLight.shadow.camera.top	= 2;
		directionalLight.shadow.camera.bottom = -2;
		// directionalLight.shadow.mapSize.width = 512;
		// directionalLight.shadow.mapSize.height = 512;
		// directionalLight.shadow.radius = 4;
		directionalLight.shadow.bias = -0.0007;

		const sunSphere = new Mesh(
		    new SphereGeometry(1),
            new MeshBasicMaterial({color: new Color('yellow')})
        );
		sunSphere.name = 'sunSphere';
		directionalLight.add(sunSphere);

		const sunPlane = new Object3D();
		sunPlane.name = 'sunPlane';
		sunPlane.position.set(-25, 0, 38);
		sunPlane.rotation.z = Math.PI / 2;
		sunPlane.add(directionalLight);

		setInterval(() => {
			if (sunPlane.rotation.z < -Math.PI / 2) sunPlane.rotation.z = Math.PI / 2;

			sunPlane.rotation.z -= 0.015;
		}, 50);

		this.scene.add( sunPlane );
	}

	onWindowResize() {
		this.camera.aspect = window.innerWidth / window.innerHeight;
		this.camera.updateProjectionMatrix();
		this.renderer.setSize( window.innerWidth, window.innerHeight );
	}

	animate(){
		requestAnimationFrame( ()=>{
			this.animate()
		});

		this.controls.update();
		this.renderer.render( this.scene, this.camera );
	}
}
